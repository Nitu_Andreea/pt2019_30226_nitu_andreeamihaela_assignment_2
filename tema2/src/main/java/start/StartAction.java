package start;

import simulator.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import objects.Magazin;
import view.*;

public class StartAction {
	private static int n =3;
	static boolean ok=true;
	private static int durataSim = 10;
	private static int maxSosire = 4;
	private static int minSosire = 1;
	private static int maxProcesare = 14;
	private static int minProcesare = 5;
	static MainView mainView; 
	
	public static void main(String[]args) {
		mainView= new MainView();//1.Se creaza vederea
		
		mainView.startBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
//2.Cand se apasa start, se creaza un nou thread de simulare
				ok=true;
				try {
				n = Integer.parseInt(mainView.nr.getText());
				if(n>10) throw new Exception("Numar de case>10");
				durataSim =Integer.parseInt(mainView.simTime.getText());
				if(!mainView.sosireInterval.getText().contains(",")) throw new Exception("Nu se gaseste virgula pentru intervalul de sosire");
				String str[]=mainView.sosireInterval.getText().split(",");
				maxSosire = Integer.parseInt(str[1]);
				minSosire = Integer.parseInt(str[0]);
				if(!mainView.procesareInterval.getText().contains(",")) throw new Exception("Nu se gaseste virgula pentru intervalul de procesare");
				String ss[]=mainView.procesareInterval.getText().split(",");
				maxProcesare = Integer.parseInt(ss[1]);
				minProcesare = Integer.parseInt(ss[0]);
				}
				catch(Exception ee) {
					JOptionPane.showMessageDialog(null,"Error:"+ee.getMessage());
					ok=false;
					
				}
				if(ok) {
				Magazin magazin=new Magazin(n);					//se creaza un nou magazin cu n cozi
				Simulare sim=new Simulare(magazin,n, durataSim, maxSosire, minSosire, maxProcesare, minProcesare);	//incepe simularea magazinului
				sim.start();}
			}

		});

		}
	
}
