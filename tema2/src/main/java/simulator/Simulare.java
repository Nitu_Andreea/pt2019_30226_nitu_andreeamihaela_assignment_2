package simulator;
import objects.*;
import view.*;
import java.awt.Color;

import java.util.Random;

import objects.Casa;
import objects.Client;
import objects.Magazin;
import view.ViewMagazin;

public class Simulare extends Thread {
	private static final int SECUNDA=1000;
	Magazin magazin;
	ViewMagazin myView; // vederea magazinului

	private int n;
	int timeLimit;
	int timp = 0;
	private int maxSosire;
	private int minSosire;
	private int maxProcesare;
	private int minProcesare;
	private int nrClienti=0;
	private static int waitingSum=0;
	int max=0;
	int min=999999;
	int timeMin=0;
	int timeMax=0;
	int tp=0; //suma pentru timpii de procesare
	int ts=0; //timp sosire suma

	public Simulare(Magazin magazin, int nr, int durataSim, int maxSosire, int minSosire, int maxProcesare,
			int minProcesare) {
		this.magazin = magazin;
		this.timeLimit = durataSim-1;
		this.maxSosire = maxSosire;
		this.minSosire = minSosire;
		this.maxProcesare = maxProcesare;
		this.minProcesare = minProcesare;
		n = nr;
		myView = new ViewMagazin(n);
		
		
		magazin.openStore(); // deschidem magazinul
	}

	public void close() {
		magazin.closeStore();
	
	}
	protected void refreshAnimation() { // reimprospateaza animatia clientilor
		for (Casa c : magazin.getCasa()) {
			myView.casa[c.getIdCasa()].setText(c.toString());
			myView.waitCasa[c.getIdCasa()].setText(c.getWaitTime() + "");
		}
	}

	public void refreshInfoAnimation(String info) { // Anunt:"Soseste clientul in x secunde"
		myView.getInfo().setForeground(Color.black);
		myView.getInfo().setText(info);

	}

	public Client clientGenerator() {
		Random r = new Random();
		int procesare = r.nextInt(maxProcesare - minProcesare + 1) + minProcesare; // +1 pt ca nu vrem sa fie timpul 0
		int sosire = r.nextInt(maxSosire - minSosire + 1) + minSosire;
		Client c = new Client(nrClienti, sosire, procesare);
		nrClienti++;
		return c;
	}

	public void run(){
		boolean ok=true;
		while (timp < timeLimit) {
				Client c=clientGenerator();						//se genereaza un client
				String info = "Vine [C " + c.getId()+"] in "+c.getTimpSosire() + " secunde! -timp procesare:"+ c.getTimpProcesare();
				tp+=c.getTimpProcesare();
				ts+=c.getTimpSosire();
				refreshInfoAnimation(info);
				ViewMagazin.getHistoryArea().append(timp+":00"+info+"\n");
				int i = 0;
				while (i < c.getTimpSosire()) {					//simulam trecerea secundelor in timp real
					if(timp>timeLimit) {ok=false; break;} //ne asiguram ca nu trecem de timpul limita
					timp += 1;
					ViewMagazin.getTimpField().setText(timp + "");
					try {
						sleep(SECUNDA);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
					int k=magazin.getNrClients();
					if(max<k) {max=k; timeMax=timp;}
					if(min>k) {min=k; timeMin=timp;}	
				}
				if (!ok) break;
				myView.getInfo().setForeground(Color.white);		//marcheaza trecerea secundelor vizual: textul din zona Info isi schimba culoarea
				magazin.aseaza(c);									//asezam clientul si reactualizam vederea magazinului
				refreshAnimation();
				myView.getInfo().setText("________________________________");
				timp++;
				ViewMagazin.getTimpField().setText(timp + "");
				try {
					sleep(SECUNDA);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		myView.getInfo().setForeground(Color.black);
		myView.getInfo().setText("____Clientii nu mai au acces!_______");
		if(timp!=timeLimit+1)timp++;
		clientsNoAcces();
		magazinInchis();
		doStatistici();	
	}
	
	private void magazinInchis() {
		myView.getInfo().setText("___________MAGAZIN INCHIS___________");
		ViewMagazin.getHistoryArea().append(timp+":00 "+"___________MAGAZIN INCHIS___________");
		refreshAnimation();
		magazin.closeStore();
	}

	private void clientsNoAcces() {
		ViewMagazin.getHistoryArea().append(timp+":00 "+"_____Clientii nu mai au acces!_______"+"\n");
		int timeStop = timp + magazin.getMaxWaitingTime();
		while (timp < timeStop) {
			
			ViewMagazin.getTimpField().setText(timp + "");
			refreshAnimation();
			timp++;
			try {
				sleep(SECUNDA);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}

	private void doStatistici() {
		ViewMagazin.getStatisticsArea().append("S-au procesat in total:"+nrClienti+"clienti.\n");
		ViewMagazin.getStatisticsArea().append("Ora de varf:"+timeMax+":00, numar de clienti:"+max+"\n");
		ViewMagazin.getStatisticsArea().append("Ora de pustietate:"+timeMin+":00, numar de clienti:"+min+"\n");
		
		double tmp=0;
		double tms=0;
		double tmw=0;
		if(nrClienti>0) {
			//System.out.println("nrClienti:"+nrClienti+ "tp= "+tp+" ts="+ts+ );
			tmp=tp/(double)nrClienti;
			tms=ts/(double)nrClienti;
			tmw=waitingSum/(double)nrClienti;
		}
		ViewMagazin.getStatisticsArea().append("Timpul de procesare total:"+tp+"\nTimpul mediu de procesare clienti"+tmp+"\n");
		ViewMagazin.getStatisticsArea().append("Timpul de sosire total:"+ts+"\nTimpul mediu de sosire clienti"+tms+"\n");
		ViewMagazin.getStatisticsArea().append("Clientii au asteptat in total:"+waitingSum+"\nTimpul mediu de asteptare al clientilor:"+tmw+"\n");
		

	}
	
	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getMaxSosire() {
		return maxSosire;
	}

	public void setMaxSosire(int maxSosire) {
		this.maxSosire = maxSosire;
	}

	public int getMinSosire() {
		return minSosire;
	}

	public void setMinSosire(int minSosire) {
		this.minSosire = minSosire;
	}

	public int getMaxProcesare() {
		return maxProcesare;
	}

	public void setMaxProcesare(int maxProcesare) {
		this.maxProcesare = maxProcesare;
	}

	public int getMinProcesare() {
		return minProcesare;
	}

	public void setMinProcesare(int minProcesare) {
		this.minProcesare = minProcesare;
	}

	public static int getWaitingSum() {
		return waitingSum;
	}

	public static void setWaitingSum(int waitingSum) {
		Simulare.waitingSum = waitingSum;
	}



}
