package view;

import java.awt.*;

import javax.swing.*;

public abstract class View {
	Dimension d = new Dimension(600, 1000);
	Font myFont = new Font("Serif", Font.BOLD, 20);
	
	//METODE SPECIFICE DE DESIGN PENTRU PROIECT
	public JPanel getBox(String s, JTextField o) {				//RETURNEAZA O COMPONENTA JPANEL, unind un textField si eticheta sa
		JLabel jl = new JLabel(s);
		jl.setFont(myFont);
		JPanel jp = new JPanel();
		jp.setBackground(new Color(249, 231, 159));
		jp.add(jl);
		jp.add((Component) o);
		jp.setSize(30, 60);
		return jp;
	}
	
	public JPanel getBox2(String s,JTextField o,JLabel waitCasa) {
		JPanel jp=getBox(s,o);
		JLabel jl2=waitCasa;
		jp.add(jl2);
		jp.setSize(30,60);
		return jp;
	}

	public JButton buttonStyle(JButton btn) {					//DESIGN BUTTON
		JButton jb = btn;
		jb.setBackground(new Color(20, 40, 128));
		jb.setFont(myFont);
		jb.setForeground(Color.white);
		return jb;
	}

	public JFrame fixFrame(String text, JPanel p) {					//RUTINA DE INITIALIZARE A FERESTREI:
		JFrame jf = new JFrame(text);											// dimensiune, imagine de fundal, culoare de fundal, vizibilitate 
		jf = new JFrame(text);															
		jf.setSize(d);
		ImageIcon i = new ImageIcon("img.png");
		JLabel photo = new JLabel();

		p.setBounds(100, 100, 400, 550);
		p.setBackground(Color.orange);
		photo.add(p);

		jf.setContentPane(photo);
		photo.setIcon(i);

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);					// si inchidere partiala a programului
		return jf;
	}

}
