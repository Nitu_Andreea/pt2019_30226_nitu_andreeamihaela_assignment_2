package view;


import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class MainView extends View {							//Creaza vederea pentru citirea datelor de intrare
	public JTextField nr = new JTextField(5);
	public JTextField simTime = new JTextField(5);
	public JTextField procesareInterval = new JTextField(5);
	public JTextField sosireInterval = new JTextField(5);
	public JButton startBtn = new JButton("             Start              ");
	JFrame main;
	
	public MainView() {
		createMainFrame();
		
	}

	private void createMainFrame() {
		JPanel mainP = new JPanel();
		mainP.setLayout(new BoxLayout(mainP, BoxLayout.Y_AXIS));		//BoxLayout
		
		Font f = new Font("Serif", Font.BOLD, 23);						//Creare titlu
		JLabel titlu = new JLabel("Main Settings for Simulation:");
		titlu.setFont(f);

		JLabel intervalTitle = new JLabel("Interval [min,max] for:");
		intervalTitle.setFont(f);

		startBtn = buttonStyle(startBtn);						//Stilizare buton

		mainP.add(titlu);
		mainP.add(new JLabel("                        "));
		mainP.add(getBox("Number of queue:", nr));
		mainP.add(getBox("Simulation time:", simTime));
		mainP.add(intervalTitle);
		mainP.add(new JLabel("                        "));
		mainP.add(getBox("arrival time:", sosireInterval));
		mainP.add(getBox("processing time:", procesareInterval));
		mainP.add(startBtn);

		main = fixFrame("Main page. Choose your setting:", mainP);
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
