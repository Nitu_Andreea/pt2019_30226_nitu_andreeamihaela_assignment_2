package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class ViewMagazin extends View {
	JFrame simulation, history, statistics;
	private int n = 3;
	private static JTextField timpField=new JTextField(5);	//timpul simularii
	public JTextField casa[];		//afisor pentru lista de clienti
	public JLabel waitCasa[];		//afisor pentru timpul de asteptare total per casa
	private JLabel info = new JLabel("SALUTARE!");	//anunta cand sosesc clienti
	
	private static JTextArea statisticsArea = new JTextArea(15, 30);	//zonele de text
	private static JTextArea historyArea = new JTextArea(15, 30);
	
	public JButton statisticsBtn = new JButton("Statistics");	//butoane ale simularii: vizualizare detalii/inchidere
	public JButton closeSim = new JButton("Close Simulation");
	

	public ViewMagazin(int n) {	//creaza vederea ce reprezinta starea magazinului
		this.n=n;
		createSimulationFrame();
		createHistoryFrame();
		createStatisticsFrame();
		waitCasa[0].setText("0");
		System.out.println(waitCasa[0].getText());
		closeSim.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("da1");
				statistics.setVisible(false);
				historyArea.setVisible(false);
				simulation.setVisible(false);
				System.exit(0);
			}
			
		});
		statisticsBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				statistics.setVisible(true);
			}
			
		});
	
	}

	public void createHistoryFrame() {
		history = createTextFrame(historyArea, "Simulation history");
		history.setLocation(600, 0);
	}
	public void createStatisticsFrame() {
		statistics = createTextFrame(statisticsArea, "Simulation statistics");
		statistics.setVisible(false);
	}

	private JFrame createTextFrame(JTextArea a, String titlu) {				//Model de creare pentru History si Statistics

		JFrame jf = new JFrame();
		
		JPanel mainP = new JPanel();
		JScrollPane js = new JScrollPane(a);
		a.setText("");
		a.setFont(new Font("Serif", Font.BOLD, 14));
		a.setForeground(Color.WHITE);
		a.setBackground(new Color(20, 40, 128));
		js.setPreferredSize(new Dimension(350, 500));
		js.setVisible(true);
		js.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);		//ScrollPane pentru vizualizarea textului intreg

		JLabel title = new JLabel(titlu);											//titlul Frame-ului
		title.setFont(myFont);
		
		mainP.add(title);
		mainP.add(js);
		
		jf = fixFrame("Read-Me", mainP);
		return jf;
	}

	private void createSimulationFrame() {									//CREARE fereastra de SIMULARE: animatie cu starea cozii
		JPanel mainP = new JPanel();

		mainP.add(getBox("Timp:", timpField));					//afisor pentru timp simulare
		
		info.setFont(new Font("Serif", Font.BOLD, 13));			//afisor Sosire Clienti
		mainP.add(info);
		
		casa = new JTextField[n];								//Crearea celor n case cu eticheta si textfield propriu, in functie de id
		waitCasa = new JLabel[n];
		
		for (int i = 0; i < n; i++) {
			waitCasa[i] = new JLabel("0");
			waitCasa[i].setFont(new Font("Serif", Font.BOLD, 13));

			casa[i] = new JTextField(30);
			casa[i].setFont(new Font("Serif", Font.BOLD, 13));
			casa[i].setEditable(false);

			String name = "C" + i + " ";
			JPanel jp = getBox2(name, casa[i], waitCasa[i]);
			mainP.add(jp);
		}

		statisticsBtn = buttonStyle(statisticsBtn);						//butoane
		closeSim = buttonStyle(closeSim);
		mainP.add(statisticsBtn);
		mainP.add(closeSim);


		simulation = fixFrame("Simulation page. After simulation you can see the detalis", mainP);

	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public static JTextField getTimpField() {
		return timpField;
	}

	public void setTimpField(JTextField timpField) {
		ViewMagazin.timpField = timpField;
	}


	public JLabel getInfo() {
		return info;
	}

	public void setInfo(JLabel info) {
		this.info = info;
	}

	public static JTextArea getStatisticsArea() {
		return statisticsArea;
	}

	public void setStatisticsArea(JTextArea statisticsArea) {
		ViewMagazin.statisticsArea = statisticsArea;
	}

	
	public static JTextArea getHistoryArea() {
		return historyArea;
	}

	public void setHistoryArea(JTextArea historyArea) {
		ViewMagazin.historyArea = historyArea;
	}
	

}
