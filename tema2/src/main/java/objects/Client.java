package objects;

public class Client {
	private String id;
	private int timpProcesare;
	private int timpSosire;
	private int timpPlecare;// pt calcul delay

	public String toString() {
		return "client:" + id + " ";
	}

	public Client(int i, int sosire, int procesare) {
		i++;
		id = i + "";
		timpProcesare = procesare;
		timpSosire = sosire;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public int getTimpProcesare() {
		return timpProcesare;
	}

	public void setTimpProcesare(int timpProcesare) {
		this.timpProcesare = timpProcesare;
	}

	public int getTimpSosire() {
		return timpSosire;
	}

	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}

	public int getTimpPlecare() {
		return timpPlecare;
	}

	public void setTimpPlecare(int tp) {
		timpPlecare = tp;
	}

}
