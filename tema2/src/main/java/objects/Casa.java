package objects;
//import simulator.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import simulator.Simulare;
import view.ViewMagazin;

public class Casa extends Thread {
	private static final int SECUNDA=1000;
	private int idCasa;
	private volatile AtomicInteger waitTime = new AtomicInteger(0);
	BlockingQueue<Client> clients;
	Boolean bol=true;

	public Casa(int i) {
		clients = new ArrayBlockingQueue<Client>(300);
		idCasa = i;
	}

	public void add(Client c) {
		c.setTimpPlecare(Integer.parseInt(ViewMagazin.getTimpField().getText())+c.getTimpProcesare());
		clients.add(c);
		addWaitTime(c.getTimpProcesare());
		String string=ViewMagazin.getTimpField().getText()+":00  La casa " + idCasa + " a ajuns [C " + c.getId()+"] cu tp="+c.getTimpProcesare();
		ViewMagazin.getHistoryArea().append(string+"\n");

	}


	public String toString() {
		String s = "";
		if (clients != null && clients.size() >= 1) {
			for (Client c : clients) {
				s += c.toString() + " ";
			}
		}
		return s;
	}

	public void run() {
		while (bol) {
			while (clients.size() >= 1) {
				try {
					Client c;
					c = clients.element();
					
					sleep(c.getTimpProcesare() * SECUNDA );
					c = clients.take();
					int delay=Integer.parseInt(ViewMagazin.getTimpField().getText())-c.getTimpPlecare();
					if(delay<0) delay=0;
					Simulare.setWaitingSum(Simulare.getWaitingSum()+delay);
					ViewMagazin.getHistoryArea().append(ViewMagazin.getTimpField().getText()+":00 [C " +c.getId()+"] a plecat din coada "+idCasa+" iesire:"+ c.getTimpPlecare()+"=>delay:"+delay+"\n");
					subWaitTime(c.getTimpProcesare());
					

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void subWaitTime(int timpProcesare) {
		waitTime.addAndGet(-timpProcesare);
	}

	public void addWaitTime(int timpProcesare) {
		waitTime.addAndGet(timpProcesare);
	}

	

	public void setId(int id) {
		this.idCasa = id;
	}
	
	public int getIdCasa() {
		return idCasa;
	}

	public int getWaitTime() {
		return waitTime.get();
	}

	public void stopCasa() {
		bol=false;
		
	}

	public void resetWaitTime() {
		waitTime.set(0);
		
	}
}
