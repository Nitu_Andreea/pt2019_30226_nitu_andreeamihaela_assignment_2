package objects;

import java.util.ArrayList;

public class Magazin {
	private ArrayList<Casa> casa = new ArrayList<Casa>();
	private int n; // nr de case

	public void closeStore() { // opreste procesul run() al caselor
		for (Casa cc : getCasa()) {
			cc.stopCasa();
			cc.resetWaitTime();

		}
	}

	public int getNrClients() {
		int k=0;
		for (Casa cc : getCasa()) {
			k+=cc.clients.size();
		}
		return k;
	}

	public void openStore() {
		for (int i = 0; i < n; i++)
			getCasa().get(i).start();
	}

	public Magazin(int n) {
		this.n = n;
		createNClasses();
	}

	public void createNClasses() {
		for (int i = 0; i < n; i++) {
			Casa c = new Casa(i);
			getCasa().add(c);
		}
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < n; i++) {
			s += i + ": " + getCasa().get(i).toString() + "\n";
		}
		return s;
	}

	public int getMinIndex() { // calculeaza indexul casei unde trebuie plasat clientul
		int min = getCasa().get(0).getWaitTime();
		int index = 0;
		for (int i = 1; i < n; i++) {
			if (getCasa().get(i).getWaitTime() < min) {
				min = getCasa().get(i).getWaitTime();
				index = i;
			}
		}
		return index;
	}

	public void aseaza(Client c) {
		int i = getMinIndex();
		getCasa().get(i).add(c);
	}

	public int getMaxWaitingTime() {
		int max = 0;
		for (Casa c : getCasa()) {
			if (c.getWaitTime() > max)
				max = c.getWaitTime();
		}
		return max;
	}

	public ArrayList<Casa> getCasa() {
		return casa;
	}

	public void setCasa(ArrayList<Casa> casa) {
		this.casa = casa;
	}

}
